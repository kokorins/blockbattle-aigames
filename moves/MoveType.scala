package moves

/**
 * MoveType class
 *
 * Enum for all possible move types
 *
 * User: goodg_000
 * Date: 12.07.2015
 * Time: 1:11
 */
sealed case class MoveType(name:String) {}

object DOWN extends MoveType("down")
object DROP extends MoveType("drop")
object LEFT extends MoveType("left")
object RIGHT extends MoveType("right")
object TURNLEFT extends MoveType("turnleft")
object TURNRIGHT extends MoveType("turnright")
object SKIP extends MoveType("skip")
