package field

/**
 * ShapeType class
 *
 * Enum for all possible Shape types
 *
 * User: goodg_000
 * Date: 06.07.2015
 * Time: 22:33
 */
sealed case class ShapeType(name: String)

object I extends ShapeType("I")
object J extends ShapeType("J")
object L extends ShapeType("L")
object O extends ShapeType("O")
object S extends ShapeType("S")
object T extends ShapeType("T")
object Z extends ShapeType("Z")
object NONE extends ShapeType("NONE")
