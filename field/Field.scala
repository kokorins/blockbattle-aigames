package field

import scala.collection.mutable.ArrayBuffer

class Field(val width: Int, val height: Int, private val grid : ArrayBuffer[ArrayBuffer[Cell]]) {
  def getCell(x: Int, y: Int) = {
    if (x < 0 || x >= width || y < 0 || y >= height) null else grid(x)(y)
  }

  def rotateClockWise() = {
    val rotGrid = ArrayBuffer.fill[Cell](height, width)(null)
    for(x<-0 until width; y<-0 until height) {
      rotGrid(y).update(x,grid(x)(height-y-1))
    }
    new Field(height, width, rotGrid)
  }

  def rotateCounterClockWise() = {
    val rotGrid:ArrayBuffer[ArrayBuffer[Cell]] = ArrayBuffer.fill[Cell](height, width)(null)
    for(x<-0 until width; y<-0 until height) {
      rotGrid(y).update(x, grid(width-x-1)(y))
    }
    new Field(height, width, rotGrid)
  }
}

object Field {
  /**
   * Parses the input string to get a grid with Cell objects
   * @param width : width of field
   * @param height : height of field
   * @param fieldString : input string
   */

  def parse(width:Int, height:Int, fieldString:String):Field = {
    val res = ArrayBuffer.fill[Cell](width, height)(null)
    val rows = fieldString.split(";")

    for (y <- 0 until height) {
      val rowCells = rows(y).split(",")

      for (x <- 0 until width) {
        val cellCode = Integer.parseInt(rowCells(x))
        res(x).update(y, new Cell(x, y, CellType.apply(cellCode)))
      }
    }
    new Field(width, height, res)
  }
}
