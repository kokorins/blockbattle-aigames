package bot

import java.awt.Point

import moves._

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
 * User: goodg_000
 * Date: 12.07.2015
 * Time: 2:23
 */
class Bot {

  def bounds(state:BotState):ArrayBuffer[Point] = {
    val field = state.myField
    val res = ArrayBuffer[Point]()
    for(x<-0 until field.width; y<- 0 until field.height) {
      if(field.getCell(x, y).isEmpty) {
        if(y==field.height-1 || !field.getCell(x, y+1).isEmpty)
          res.insert(res.size-1, field.getCell(x,y).location)
      }
    }
    res
  }

  def getMoves(state: BotState, timeout: Long): List[MoveType] = {
    val res = new ArrayBuffer[MoveType]()

    val allMoves = Array(DOWN, LEFT, RIGHT, TURNLEFT, TURNRIGHT)
    val rnd = new Random()

    val numberOfMoves = rnd.nextInt(41)

    for (i <- 0 until numberOfMoves) {
      res += allMoves(rnd.nextInt(allMoves.length))
    }

    res.toList
  }
}
